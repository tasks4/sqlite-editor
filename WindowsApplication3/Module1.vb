﻿Imports System.Data.SQLite

Module Module1
    Public connection As New SQLiteConnection("Data Source=C:\sqlite\db\test.db; Version=3")

    Public Sub reciveTable(ByVal grid As DataGridView, sqlCommand As String)
        connection.Open()
        Dim cmd As New SQLiteCommand
        cmd.Connection = connection

        cmd.CommandText = sqlCommand
        Dim reader As SQLite.SQLiteDataReader = cmd.ExecuteReader
        Dim dataTable As New DataTable
        dataTable.Load(reader)
        reader.Close()
        connection.Close()
        grid.DataSource = dataTable

    End Sub

    Public Sub runCommand(sqlCommand As String)
        connection.Open()
        Dim cmd As New SQLiteCommand
        cmd.Connection = connection
        cmd.CommandText = sqlCommand
        cmd.ExecuteNonQuery()
        connection.Close()

    End Sub

    Public Sub saveUserInformation(ByVal grid As DataGridView)
        connection.Open()

        Dim cmd As New SQLiteCommand
        cmd.Connection = connection

        cmd.CommandText = "INSERT OR REPLACE INTO users (id, first_name, last_name, username, password, status )" &
                           "VALUES (@id,@first_name, @last_name, @username, @password, @status)"
        cmd.Prepare()
        Dim id As String
        Dim first_name As String
        Dim last_name As String
        Dim username As String
        Dim password As String
        Dim status As String


        id = grid.CurrentRow.Cells("id").Value
        first_name = grid.CurrentRow.Cells("first_name").Value
        last_name = grid.CurrentRow.Cells("last_name").Value
        username = grid.CurrentRow.Cells("username").Value
        password = grid.CurrentRow.Cells("password").Value
        status = grid.CurrentRow.Cells("status").Value

        cmd.Parameters.AddWithValue("@id", id)
        cmd.Parameters.AddWithValue("@first_name", first_name)
        cmd.Parameters.AddWithValue("@last_name", last_name)
        cmd.Parameters.AddWithValue("@username", username)
        cmd.Parameters.AddWithValue("@password", password)
        cmd.Parameters.AddWithValue("@status", CInt(status))

        cmd.ExecuteNonQuery()
        connection.Close()
    End Sub



    Public Sub saveProductInformation(ByVal grid As DataGridView)
        connection.Open()

        Dim cmd As New SQLiteCommand
        cmd.Connection = connection

        cmd.CommandText = "INSERT OR REPLACE INTO products (id, name, description, short_description, price )" &
                           "VALUES (@id, @name, @description, @short_description, @price)"
        cmd.Prepare()
        Dim id As String
        Dim name As String
        Dim description As String
        Dim short_description As String
        Dim price As String


        id = grid.CurrentRow.Cells("id").Value
        name = grid.CurrentRow.Cells("name").Value
        description = grid.CurrentRow.Cells("description").Value
        short_description = grid.CurrentRow.Cells("short_description").Value
        price = grid.CurrentRow.Cells("price").Value

        cmd.Parameters.AddWithValue("@id", id)
        cmd.Parameters.AddWithValue("@name", name)
        cmd.Parameters.AddWithValue("@description", description)
        cmd.Parameters.AddWithValue("@short_description", short_description)
        cmd.Parameters.AddWithValue("@price", price)

        cmd.ExecuteNonQuery()
        connection.Close()
    End Sub
End Module
